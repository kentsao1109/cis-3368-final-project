CREATE TABLE public.test
(
    id serial PRIMARY KEY NOT NULL,
    name varchar(20)
);
CREATE UNIQUE INDEX test_id_uindex ON public.test (id);