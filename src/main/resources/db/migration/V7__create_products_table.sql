CREATE TABLE public.products
(
    product_id serial PRIMARY KEY NOT NULL,
    product_name VARCHAR(50) NOT NULL,
    product_cost VARCHAR(20) NOT NULL,
    product_description VARCHAR(100000) NOT NULL
);
CREATE UNIQUE INDEX products_product_id_uindex ON public.products (product_id);