CREATE TABLE public.employee
(
    employee_id int PRIMARY KEY NOT NULL,
    first_name varchar(24),
    last_name varchar(24),
    email varchar(50),
    phone_number varchar(12)
);