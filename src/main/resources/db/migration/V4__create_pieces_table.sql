CREATE TABLE public.pieces
(
    id serial PRIMARY KEY NOT NULL,
    cost int NOT NULL
);
CREATE UNIQUE INDEX pieces_id_uindex ON public.pieces (id);