CREATE TABLE public.archived
(
    archived_id serial PRIMARY KEY NOT NULL,
    archived_name varchar(200) NOT NULL,
    archived_money VARCHAR(200) NOT NULL,
    archived_date varchar(10) NOT NULL
);
CREATE UNIQUE INDEX archived_archived_id_uindex ON public.archived (archived_id);