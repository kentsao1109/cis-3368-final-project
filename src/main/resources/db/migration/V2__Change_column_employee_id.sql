CREATE SEQUENCE public.employee_employee_id_seq NO MINVALUE NO MAXVALUE NO CYCLE;
ALTER TABLE public.employee ALTER COLUMN employee_id SET DEFAULT nextval('public.employee_employee_id_seq');
ALTER SEQUENCE public.employee_employee_id_seq OWNED BY public.employee.employee_id;