CREATE TABLE public.component
(
    component_id serial PRIMARY KEY NOT NULL,
    component_name varchar(50) NOT NULL,
    cost varchar(24) NOT NULL
);
CREATE UNIQUE INDEX component_component_id_uindex ON public.component (component_id);