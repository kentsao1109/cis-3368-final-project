package edu.uh.tech.cis3368.manufacturingproject;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class MainController implements CommandLineRunner {

    public ManufacturingprojectApplication main;
    Parent root;
    private Stage primaryStage;

    @Autowired
    EmployeeRepository employeeRepository;


    @FXML
    public void initialize() {

    }

    /**
     * @throws IOException
     * Function calls showCustomProductsScreen to load
     * and display the Request Custom Products form
     */
    @FXML
    private void goCustomProductsScreen() throws IOException{
        main.showCustomProductsScreen();
    }

    /**
     * @throws IOException
     * Function calls showComponentsScreen to load
     * and display the Components form
     */
    @FXML
    private void goViewItems() throws IOException{
        main.showComponentsScreen();
    }

    /**
     * @throws IOException
     * Function calls showArchivedScreeen to load
     * and display the items in archived
     */
    @FXML
    private void goArchiveScreen() throws IOException{
        main.showArchivedScreeen();
    }

    /**
     * @throws IOException
     * Function calls showKanbanScreen to load
     * and display the Kanban board
     */
    @FXML
    private void goKanbanBoard() throws IOException{
        main.showKanbanScreen();
    }

    /**
     * @throws IOException
     * Function calls showEmployeeScreen to load and
     * display the employee screen
     */
    @FXML
    private void goEmployee() throws IOException {
       main.showEmployeeScreen();
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
