package edu.uh.tech.cis3368.manufacturingproject;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ComponentsScreenController implements CommandLineRunner {

    @FXML
    private TextField name;
    @FXML
    private TextField cost;
    @FXML
    private TableView<Components> table;
    @FXML
    private TableColumn<Components, String> col_name;
    @FXML
    private TableColumn<Components, String> col_cost;
    @Autowired
    ComponentsRepository componentsRepository;

    public ManufacturingprojectApplication main;

    ObservableList<Components> componentsList = FXCollections.observableArrayList();

    /**
     * Initializes the Components Screen and sets up
     * table columns and populates the tableview with
     * table values
     */
    @FXML
    private void initialize(){
        //set up table columns
        col_name.setCellValueFactory(new PropertyValueFactory<>("componentName"));
        col_cost.setCellValueFactory(new PropertyValueFactory<>("cost"));


        loadData();
        table.setItems(componentsList);
        table.setEditable(true);
        col_name.setCellFactory(TextFieldTableCell.forTableColumn());
        col_cost.setCellFactory(TextFieldTableCell.forTableColumn());

    }

    /**
     * Function loads components from the components table
     * and adds it to the componentsList
     */
    public void loadData(){
        for(Components components: componentsRepository.findAll()) {
            if(!componentsList.contains(components))
            {
                componentsList.add(new Components(components.getComponentId(), components.getComponentName(), components.getCost()));
            }
        }
    }

    /**
     * @param actionEvent
     * Function adds a new component to the components table as long as
     * the name and cost fields are not empty
     */
    public void addBtn(ActionEvent actionEvent){
        if(!name.getText().equals("") && !cost.getText().equals("")){
            Components component = new Components(name.getText(), cost.getText());
            componentsRepository.save(component);
            System.out.println("Item added");

            loadData();
        }

    }

    /**
     * @param editted
     * Function allows for double click edit functionality to edit the name of the component
     * Edits are saved into the components table and updates
     */
    public void changeNameCellEvent(TableColumn.CellEditEvent editted)
    {
        Components componentsSelected = table.getSelectionModel().getSelectedItem();
        componentsSelected.setComponentName(editted.getNewValue().toString());
        Components components = componentsRepository.findById(componentsSelected.getComponentId());
        components.setComponentName(componentsSelected.getComponentName());
        componentsRepository.save(components);
    }

    /**
     * @param editted
     * Function allows for double click edit functionality to edit the cost of the component
     * Edits are saved into the components table and updates
     */
    public void changeCostCellEvent(TableColumn.CellEditEvent editted)
    {
        Components componentsSelected = table.getSelectionModel().getSelectedItem();
        componentsSelected.setCost(editted.getNewValue().toString());
        Components components = componentsRepository.findById(componentsSelected.getComponentId());
        components.setCost(componentsSelected.getCost());
        componentsRepository.save(components);
    }

    /**
     * Function deletes selected row from the componentsList
     * and components table
     */
    public void deleteData(){
        ObservableList<Components> selectedRow;

        selectedRow = table.getSelectionModel().getSelectedItems();

        for(Components component: selectedRow)
        {
            componentsList.remove(component);
            componentsRepository.delete(component);
        }
    }

    /**
     * @param actionEvent
     * Function calls deleteData() to delete selectedRow
     */
    @FXML
    public void deleteButton(ActionEvent actionEvent){
        deleteData();
    }

    /**
     * @param actionEvent
     * Function takes in an ActionEvent parameter
     * @throws IOException
     * Function returns user to the main.fxml screen
     */
    public void goBackBtn(ActionEvent actionEvent) throws IOException {
        main.showMainScreen();
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
