package edu.uh.tech.cis3368.manufacturingproject;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class EmployeeScreenController implements CommandLineRunner{

    @Autowired
    EmployeeRepository employeeRepository;

    public ManufacturingprojectApplication main;


    @FXML
    private TableView<Employee> table;
    @FXML
    private TableColumn<Employee, String> col_first_name;
    @FXML
    private TableColumn<Employee, String> col_last_name;
    @FXML
    private TableColumn<Employee, String> col_phone;
    @FXML
    private TableColumn<Employee, String> col_email;


    ObservableList<Employee> employeeList = FXCollections.observableArrayList();

    /**
     * Function populates the table with employeeList and sets
     * each cell value
     */
    @FXML
    private void initialize(){
        //set up table columns

        col_first_name.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        col_last_name.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        col_phone.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        col_email.setCellValueFactory(new PropertyValueFactory<>("email"));

        loadData();
        table.setItems(employeeList);
        table.setEditable(true);
        col_first_name.setCellFactory(TextFieldTableCell.forTableColumn());
        col_last_name.setCellFactory(TextFieldTableCell.forTableColumn());
        col_phone.setCellFactory(TextFieldTableCell.forTableColumn());
        col_email.setCellFactory(TextFieldTableCell.forTableColumn());
    }

    /**
     * Function loads data from the employee table and adds all employees
     * to the observablelist employeeList if employee is not already in the list.
     */
    public void loadData(){
        for(Employee employee: employeeRepository.findAll()) {
            if(!employeeList.contains(employee))
            {
                employeeList.add(new Employee(employee.getEmployeeId(), employee.getFirstName(), employee.getLastName(), employee.getEmail(), employee.getPhoneNumber()));
            }
        }

    }

    /**
     * @param editted
     * Allows user to update the first name column of the employee table by double clicking
     * in a cell and entering a new value and pressing enter
     */
    public void changeFirstNameCellEvent(TableColumn.CellEditEvent editted)
    {
        Employee employeeSelected = table.getSelectionModel().getSelectedItem();
        employeeSelected.setFirstName(editted.getNewValue().toString());
        Employee employee = employeeRepository.findById(employeeSelected.getEmployeeId());
        employee.setFirstName(employeeSelected.getFirstName());
        employeeRepository.save(employee);
    }

    /**
     * @param editted
     * Allows user to update the last name column of the employee table by double clicking
     * in a cell and entering a new value and pressing enter
     */
    public void changeLastNameCellEvent(TableColumn.CellEditEvent editted)
    {
        Employee employeeSelected = table.getSelectionModel().getSelectedItem();
        employeeSelected.setLastName(editted.getNewValue().toString());
        Employee employee = employeeRepository.findById(employeeSelected.getEmployeeId());
        employee.setLastName(employeeSelected.getLastName());
        employeeRepository.save(employee);
    }

    /**
     * @param editted
     * Allows user to update the email column of the employee table by double clicking
     * in a cell and entering a new value and pressing enter
     */
    public void changeEmailCellEvent(TableColumn.CellEditEvent editted)
    {
        Employee employeeSelected = table.getSelectionModel().getSelectedItem();
        employeeSelected.setEmail(editted.getNewValue().toString());
        Employee employee = employeeRepository.findById(employeeSelected.getEmployeeId());
        employee.setEmail(employeeSelected.getEmail());
        employeeRepository.save(employee);
    }

    /**
     * @param editted
     * Allows user to update the phone column of the employee table by double clicking
     * in a cell and entering a new value and pressing enter
     */
    public void changePhoneCellEvent(TableColumn.CellEditEvent editted)
    {
        Employee employeeSelected = table.getSelectionModel().getSelectedItem();
        employeeSelected.setPhoneNumber(editted.getNewValue().toString());
        Employee employee = employeeRepository.findById(employeeSelected.getEmployeeId());
        employee.setPhoneNumber(employeeSelected.getPhoneNumber());
        employeeRepository.save(employee);
    }

    /**
     * Function deletes selected employee from the employee table
     * and employeeList
     */
    public void deleteData(){
        ObservableList<Employee> selectedRow;

        selectedRow = table.getSelectionModel().getSelectedItems();
        for(Employee employee: selectedRow)
        {
            employeeList.remove(employee);
            employeeRepository.delete(employee);
        }
    }

    /**
     * @param actionEvent
     * Function calls deleteData()
     */
    @FXML
    public void deleteButton(ActionEvent actionEvent){
        deleteData();
    }

    /**
     * @throws IOException
     * Function takes user to the add employee screen
     */
    @FXML
    private void goAddEmployeeScreen() throws IOException {
        main.showEmployeeAddScreen();
    }

    /**
     * @throws IOException
     * Function returns user to the home screen
     */
    @FXML
    private void goHomeScreen() throws IOException {
        main.showMainScreen();
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
