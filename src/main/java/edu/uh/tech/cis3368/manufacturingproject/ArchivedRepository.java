package edu.uh.tech.cis3368.manufacturingproject;

import org.springframework.data.repository.CrudRepository;

public interface ArchivedRepository extends CrudRepository<Archived, Integer> {
}
