package edu.uh.tech.cis3368.manufacturingproject;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Archived {
    private int archivedId;
    private String archivedName;
    private String archivedMoney;
    private String archivedDate;

    public Archived(){

    }

    public Archived(String archivedName, String archivedMoney, String archivedDate){
        this.archivedName = archivedName;
        this.archivedMoney = archivedMoney;
        this.archivedDate = archivedDate;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "archived_id", nullable = false)
    public int getArchivedId() {
        return archivedId;
    }

    public void setArchivedId(int archivedId) {
        this.archivedId = archivedId;
    }

    @Basic
    @Column(name = "archived_name", nullable = false, length = 200)
    public String getArchivedName() {
        return archivedName;
    }

    public void setArchivedName(String archivedName) {
        this.archivedName = archivedName;
    }

    @Basic
    @Column(name = "archived_money", nullable = false, length = 200)
    public String getArchivedMoney() {
        return archivedMoney;
    }

    public void setArchivedMoney(String archivedMoney) {
        this.archivedMoney = archivedMoney;
    }

    @Basic
    @Column(name = "archived_date", nullable = false, length = 10)
    public String getArchivedDate() {
        return archivedDate;
    }

    public void setArchivedDate(String archivedDate) {
        this.archivedDate = archivedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Archived archived = (Archived) o;
        return archivedId == archived.archivedId &&
                Objects.equals(archivedName, archived.archivedName) &&
                Objects.equals(archivedMoney, archived.archivedMoney) &&
                Objects.equals(archivedDate, archived.archivedDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(archivedId, archivedName, archivedMoney, archivedDate);
    }
}
