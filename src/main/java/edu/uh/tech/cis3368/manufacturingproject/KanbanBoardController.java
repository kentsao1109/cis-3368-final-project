package edu.uh.tech.cis3368.manufacturingproject;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

@Component
public class KanbanBoardController implements CommandLineRunner {

    @Autowired
    ProductsRepository productsRepository;
    @Autowired
    ArchivedRepository archivedRepository;

    public ManufacturingprojectApplication main;
    @FXML
    private TableView<Products> table1, table2, table3;
    @FXML
    private TableColumn<Products, String> col1_name, col2_name, col3_name;


    ObservableList<Products> stage1List = FXCollections.observableArrayList();
    ObservableList<Products> stage2List = FXCollections.observableArrayList();
    ObservableList<Products> stage3List = FXCollections.observableArrayList();

    private static final DataFormat STAGE1_LIST = new DataFormat("cis3368/stage1List");
    private static final DataFormat STAGE2_LIST = new DataFormat("cis3368/stage2List");
    private static final DataFormat STAGE3_LIST = new DataFormat("cis3368/stage3List");

    /**
     * Function initializes the three tables, populates them, and sets
     * tablecolumn values
     */
    @FXML
    public void initialize(){
        col1_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        col2_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        col3_name.setCellValueFactory(new PropertyValueFactory<>("productName"));

        loadData();
        table1.setItems(stage1List);
        table2.setItems(stage2List);
        table3.setItems(stage3List);
        col1_name.setCellFactory(TextFieldTableCell.forTableColumn());
        col2_name.setCellFactory(TextFieldTableCell.forTableColumn());
        col3_name.setCellFactory(TextFieldTableCell.forTableColumn());

    }

    /**
     * @param mouseEvent
     * Function detects drag from table1
     */
    public void onDragDetected(MouseEvent mouseEvent){
        System.out.println("Drag detected");
        Dragboard dragboard = table1.startDragAndDrop(TransferMode.MOVE);
        ArrayList<Products> selectedItems = new ArrayList<>(table1.getSelectionModel().getSelectedItems());
        ClipboardContent clipboardContent = new ClipboardContent();
        clipboardContent.put(STAGE1_LIST, selectedItems);
        dragboard.setContent(clipboardContent);

        mouseEvent.consume();
    }

    /**
     * @param mouseEvent
     * Function detects drag from table2
     */
    public void onDrag2Detected(MouseEvent mouseEvent){
        System.out.println("Drag detected");
        Dragboard dragboard = table2.startDragAndDrop(TransferMode.MOVE);
        ArrayList<Products> selectedItems = new ArrayList<>(table2.getSelectionModel().getSelectedItems());
        ClipboardContent clipboardContent = new ClipboardContent();
        clipboardContent.put(STAGE2_LIST, selectedItems);
        dragboard.setContent(clipboardContent);

        mouseEvent.consume();
    }

    /**
     * @param mouseEvent
     * Function detects drag from table3
     */
    public void onDrag3Detected(MouseEvent mouseEvent){
        System.out.println("Drag detected");
        Dragboard dragboard = table3.startDragAndDrop(TransferMode.MOVE);
        ArrayList<Products> selectedItems = new ArrayList<>(table3.getSelectionModel().getSelectedItems());
        ClipboardContent clipboardContent = new ClipboardContent();
        clipboardContent.put(STAGE3_LIST, selectedItems);
        dragboard.setContent(clipboardContent);

        mouseEvent.consume();
    }

    /**
     * @param dragEvent
     * Function detects when you drag over a table and sets dragevent
     * to accept any mode of transfer
     */
    public void onDragOver(DragEvent dragEvent) {
        Dragboard dragboard = dragEvent.getDragboard();
        if (dragboard.hasContent(STAGE1_LIST) || dragboard.hasContent(STAGE2_LIST) || dragboard.hasContent(STAGE3_LIST)) {
            dragEvent.acceptTransferModes(TransferMode.ANY);
        }
        dragEvent.consume();
    }

    /**
     * @param dragEvent
     * Function adds product from table1 to table2 when drag is dropped in table2
     * and updates the stage of the item dropped from 1 to 2
     */
    public void onDragDrop(DragEvent dragEvent){
        boolean dragCompleted = false;
        Dragboard dragboard = dragEvent.getDragboard();
        if(dragboard.hasContent(STAGE1_LIST)){
            ArrayList<Products> productsDragged = (ArrayList<Products>)dragboard.getContent(STAGE1_LIST);
            productsDragged.forEach(products -> {
                System.out.println(products.getStage());
                if(products.getStage() == 1) {
                    products.setStage(2);
                    productsRepository.save(products);
                    stage1List.remove(products);
                    stage2List.add(products);
                }
            });
            dragCompleted = true;
        }
        dragEvent.setDropCompleted(dragCompleted);
        dragEvent.consume();
    }

    /**
     * @param dragEvent
     * Function adds product from table2 to table3 when drag is dropped in table3
     * and updates stage of the item dropped from 2 to 3
     */
    public void onDrag2Drop(DragEvent dragEvent){
        boolean dragCompleted = false;
        Dragboard dragboard = dragEvent.getDragboard();
        if(dragboard.hasContent(STAGE2_LIST)){
            ArrayList<Products> productsDragged = (ArrayList<Products>)dragboard.getContent(STAGE2_LIST);
            productsDragged.forEach(products -> {
                if(products.getStage() == 2){
                    products.setStage(3);
                    productsRepository.save(products);
                    stage2List.remove(products);
                    stage3List.add(products);
                }
            });
            dragCompleted = true;

        }
        dragEvent.setDropCompleted(dragCompleted);
        dragEvent.consume();
    }

    /**
     * Function loads data from the products table into three separate lists based
     * on the stage column value
     */
    public void loadData() {
        for (Products products : productsRepository.findAll()) {
            if (!stage1List.contains(products) && products.getStage() == 1) {
                stage1List.add(new Products(products.getProductId(), products.getProductName(), products.getProductCost(), products.getProductDescription(), products.getStage()));
            }
        }
        for(Products products : productsRepository.findAll())
        {
            if(!stage2List.contains(products) && products.getStage() == 2)
            {
                stage2List.add(new Products(products.getProductId(), products.getProductName(), products.getProductCost(), products.getProductDescription(), products.getStage()));
            }
        }
        for(Products products : productsRepository.findAll())
        {
            if(!stage3List.contains(products) && products.getStage() == 3)
            {
                stage3List.add(new Products(products.getProductId(), products.getProductName(), products.getProductCost(), products.getProductDescription(), products.getStage()));
            }
        }
    }

    /**
     * @param actionEvent
     * Function adds selected item to archived table and records the date the item was archived
     */
    public void archiveBtn(ActionEvent actionEvent){
        ObservableList<Products> selectedRow;

        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        String strDate = dateFormat.format(date);
        selectedRow = table3.getSelectionModel().getSelectedItems();
        for(Products products: selectedRow)
        {
            stage3List.remove(products);
            Archived archived = new Archived(products.getProductName(), products.getProductCost(), strDate);
            archivedRepository.save(archived);
            productsRepository.delete(products);
        }
    }

    /**
     * @param actionEvent
     * takes in ActionEvent parameter
     * @throws IOException
     * Function returns user to the main screen page
     */
    public void backBtn(ActionEvent actionEvent) throws IOException {
        main.showMainScreen();
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
