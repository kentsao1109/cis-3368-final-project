package edu.uh.tech.cis3368.manufacturingproject;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;

@SpringBootApplication
public class ManufacturingprojectApplication extends Application implements CommandLineRunner{

    @Autowired
    private static ConfigurableApplicationContext springContext;

    private static Parent root;
    private static Stage primaryStage;
    private static BorderPane mainLayout;

    public static void main(String[] args) {
        launch();
    }

    /**
     * @throws Exception
     * Loads the Login screen
     */
    @Override
    public void init() throws Exception {
        springContext = SpringApplication.run(ManufacturingprojectApplication.class);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("LoginScreen.fxml"));
        fxmlLoader.setControllerFactory(springContext::getBean);
        root = fxmlLoader.load();
        super.init();
    }

    /**
     * @param stage
     * sets the stage to Login Screen
     * @throws Exception
     * sets the stage to Login Screen
     * and displays
     */
    @Override
    public void start(Stage stage) throws Exception {
        stage.setScene(new Scene(root, 600, 400));
        primaryStage = stage;
        System.out.println("Showing page");
        primaryStage.show();
    }

    /**
     * @throws IOException
     * Loads and displays the main screen
     */
    public static void showMainScreen() throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(ManufacturingprojectApplication.class.getResource("main.fxml"));
        fxmlLoader.setControllerFactory(springContext::getBean);
        root = fxmlLoader.load();
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
        System.out.println("Showing Main Screen");
    }

    /**
     * @throws IOException
     * loads and displays the archived screen
     */
    public static void showArchivedScreeen() throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(ManufacturingprojectApplication.class.getResource("ArchiveScreen.fxml"));
        fxmlLoader.setControllerFactory(springContext::getBean);
        root = fxmlLoader.load();
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
        System.out.println("Showing Archive Screen");
    }

    /**
     * @throws IOException
     * loads and displays the custom products screen
     */
    public static void showCustomProductsScreen() throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(ManufacturingprojectApplication.class.getResource("CustomProductScreen.fxml"));
        fxmlLoader.setControllerFactory(springContext::getBean);
        root = fxmlLoader.load();
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
        System.out.println("Showing Custom Product Screen");
    }

    /**
     * @throws IOException
     * loads and displays the components screen
     */
    public static void showComponentsScreen() throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(ManufacturingprojectApplication.class.getResource("ComponentsScreen.fxml"));
        fxmlLoader.setControllerFactory(springContext::getBean);
        root = fxmlLoader.load();
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
        System.out.println("Showing Components Screen");
    }

    /**
     * @throws IOException
     * loads and displays the employee screen
     */
    public static void showEmployeeScreen() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(ManufacturingprojectApplication.class.getResource("EmployeeScreen.fxml"));
        fxmlLoader.setControllerFactory(springContext::getBean);
        root = fxmlLoader.load();
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
        System.out.println("Showing Employee Screen");
    }

    /**
     *
     * @throws IOException
     * loads and displays the kanban board
     */
    public static void showKanbanScreen() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(ManufacturingprojectApplication.class.getResource("KanbanBoard.fxml"));
        fxmlLoader.setControllerFactory(springContext::getBean);
        root = fxmlLoader.load();
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
        System.out.println("Showing Kanban Board Screen");
    }

    /**
     * @throws IOException
     * loads and displays the add employee screen
     */
    public static void showEmployeeAddScreen() throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(ManufacturingprojectApplication.class.getResource("EmployeeAddScreen.fxml"));
        fxmlLoader.setControllerFactory(springContext::getBean);
        root = fxmlLoader.load();
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
        System.out.println("Showing Employee Add Screen");
    }


    @Override
    public void run(String... args) throws Exception {

    }

}
