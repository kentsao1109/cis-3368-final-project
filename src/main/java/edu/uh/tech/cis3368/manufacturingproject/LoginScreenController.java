package edu.uh.tech.cis3368.manufacturingproject;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class LoginScreenController implements CommandLineRunner {

    public ManufacturingprojectApplication main;

    @FXML
    private TextField user;
    @FXML
    private PasswordField pw;

    /**
     * @param actionEvent
     * takes in actionEvent parameter
     * @throws IOException
     * Function takes in textfield and passwordfield user and pw
     * and checks that they are both "admin" when the login button is clicked
     * If login button is clicked and conditionals are met, takes user to the
     * main screen
     */
    @FXML
    public void loginButton(ActionEvent actionEvent) throws IOException {
        if(user.getText().equals("admin") && pw.getText().equals("admin")){
            main.showMainScreen();
        }

    }


    @Override
    public void run(String... args) throws Exception {

    }
}
