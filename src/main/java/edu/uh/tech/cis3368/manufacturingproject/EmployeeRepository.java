package edu.uh.tech.cis3368.manufacturingproject;

import javafx.collections.ObservableList;
import org.springframework.data.annotation.Id;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EmployeeRepository extends CrudRepository<Employee,Integer> {
    List<Employee> findByFirstName(String name);

    Employee findById(int id);
}
