package edu.uh.tech.cis3368.manufacturingproject;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class CustomProductScreenController implements CommandLineRunner {

    @FXML
    private TextField name, cost;
    @FXML
    private TextArea description;
    @FXML
    private TableView<Components> table1, table2;
    @FXML
    private TableColumn<Components, String> col1_name, col2_name;
    @FXML
    private TableColumn<Components, String> col1_cost, col2_cost;

    @Autowired
    ComponentsRepository componentsRepository;
    @Autowired
    ProductsRepository productsRepository;

    double currentcost, totalcost;
    String total;

    public ManufacturingprojectApplication main;

    ObservableList<Components> componentsList = FXCollections.observableArrayList();
    ObservableList<Components> addList = FXCollections.observableArrayList();

    /**
     * Function sets up tablecolumns and populates tableview
     * with data from the componentsList
     */
    @FXML
    private void initialize(){
        //set up table columns
        col1_name.setCellValueFactory(new PropertyValueFactory<>("componentName"));
        col1_cost.setCellValueFactory(new PropertyValueFactory<>("cost"));
        col2_name.setCellValueFactory(new PropertyValueFactory<>("componentName"));
        col2_cost.setCellValueFactory(new PropertyValueFactory<>("cost"));


        loadData();
        table1.setItems(componentsList);
        col1_name.setCellFactory(TextFieldTableCell.forTableColumn());
        col1_cost.setCellFactory(TextFieldTableCell.forTableColumn());
        col2_name.setCellFactory(TextFieldTableCell.forTableColumn());
        col2_cost.setCellFactory(TextFieldTableCell.forTableColumn());

    }

    /**
     * Function adds selected component to the addList and populates table2
     * Contains calculation of estimated cost of product requested with a formula
     * and populates the cost field
     */
    public void addBtn(){
        ObservableList<Components> selectedRow;

        totalcost = 0;
        selectedRow = table1.getSelectionModel().getSelectedItems();
        for(Components component: selectedRow){
            addList.add(component);
        }

       for(Components component: addList) {
           totalcost += Double.valueOf(component.getCost());
           currentcost = (double)Math.round((totalcost * .10 + totalcost * .034 + totalcost)*100)/100;
           total = Double.toString(currentcost);
           cost.setText(total);
       }

        table2.setItems(addList);

    }

    /**
     * Function deletes selected component from addlist and
     * repopulates table2 with the new addlist
     */
    public void deleteBtn(){
        ObservableList<Components> selectedRow;

        selectedRow = table2.getSelectionModel().getSelectedItems();

        selectedRow.forEach((Components comp) -> {
            addList.remove(comp);
        });

        table2.setItems(addList);
    }

    /**
     * Function fully clears addList
     */
    public void clearBtn(){
        addList.clear();
    }

    /**
     * Function saves name, cost, and description textfields into the products table
     */
    public void requestBtn(){
        Products product = new Products(name.getText(), cost.getText(), description.getText(), 1);
        productsRepository.save(product);
        name.setText("");
        cost.setText("");
        description.setText("");
    }

    /**
     * Function loads data from the components field into an observablelist
     * componentsList
     */
    public void loadData() {
        for (Components components : componentsRepository.findAll()) {
            if (!componentsList.contains(components)) {
                componentsList.add(new Components(components.getComponentId(), components.getComponentName(), components.getCost()));
            }
        }
    }

    /**
     * @throws IOException
     * Function returns user to main screen
     */
    public void backBtn() throws IOException {
        main.showMainScreen();
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
