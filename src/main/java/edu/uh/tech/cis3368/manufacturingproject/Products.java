package edu.uh.tech.cis3368.manufacturingproject;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Products implements Serializable {
    private int productId;
    private String productName;
    private String productCost;
    private String productDescription;
    private int stage;

    public Products(){

    }
    public Products(int productId, String productName, String productCost, String productDescription){
        this.productId = productId;
        this.productName = productName;
        this.productCost = productCost;
        this.productDescription = productDescription;
    }

  /*  public Products(int productId, String productName, String productCost, String productDescription, int productStage){
        this.productId = productId;
        this.productName = productName;
        this.productCost = productCost;
        this.productDescription = productDescription;
        this.productStage = productStage;
    }*/

    public Products(int productId, String productName, String productCost, String productDescription, int stage){
        this.productId = productId;
        this.productName = productName;
        this.productCost = productCost;
        this.productDescription = productDescription;
        this.stage = stage;
    }

    public Products(String productName, String productCost, String productDescription, int productStage){
        this.productName = productName;
        this.productCost = productCost;
        this.productDescription = productDescription;
        this.stage = productStage;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id", nullable = false)
    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Basic
    @Column(name = "product_name", nullable = false, length = 50)
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Basic
    @Column(name = "product_cost", nullable = false, length = 20)
    public String getProductCost() {
        return productCost;
    }

    public void setProductCost(String productCost) {
        this.productCost = productCost;
    }


    @Basic
    @Column(name = "product_description", nullable = false, length = 100000)
    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Products products = (Products) o;
        return productId == products.productId &&
                Objects.equals(productName, products.productName) &&
                Objects.equals(productCost, products.productCost) &&
                Objects.equals(productDescription, products.productDescription);
    }

    @Override
    public int hashCode() {

        return Objects.hash(productId, productName, productCost, productDescription);
    }

    @Basic
    @Column(name = "stage", nullable = false)
    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }
}
