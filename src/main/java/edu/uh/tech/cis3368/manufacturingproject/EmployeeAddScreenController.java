package edu.uh.tech.cis3368.manufacturingproject;

import javafx.fxml.FXML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.awt.*;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javax.swing.*;
import java.io.IOException;

@Component
public class EmployeeAddScreenController implements CommandLineRunner{

    public ManufacturingprojectApplication main;

    @FXML
    private TextField first_name;
    @FXML
    private TextField last_name;
    @FXML
    private TextField email;
    @FXML
    private TextField phone_number;

    @Autowired
    EmployeeRepository employeeRepository;

    /**
     * @param actionEvent
     * Function creates an employee filled with first_name, last_name, email, and phone_number
     * textfields and saves that employee to the employee table. Conditions are that the textfields
     * are not empty.
     */
    public void AddButton(ActionEvent actionEvent){
        if(!first_name.getText().equals("") && !last_name.getText().equals("") && !email.getText().equals("") && !phone_number.equals("")){
            Employee employee = new Employee(first_name.getText(), last_name.getText(), email.getText(), phone_number.getText());
            employeeRepository.save(employee);
            System.out.println("Employee added");
        }
        else
        {
            first_name.setText("Error: Please enter first name");
            last_name.setText("Error: Please enter last name");
            email.setText("Error: Please enter email");
            phone_number.setText("Error: Please enter phone number");

        }
    }

    /**
     * @param actionEvent
     * Function takes an ActionEvent parameter
     * @throws IOException
     * Function returns user to the employee list screen
     */
    @FXML
    private void backButton(ActionEvent actionEvent) throws IOException {
        main.showEmployeeScreen();
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
