package edu.uh.tech.cis3368.manufacturingproject;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ArchiveScreenController implements CommandLineRunner {

    @FXML
    private TableView<Archived> table;
    @FXML
    private TableColumn<Archived, String> col_name;
    @FXML
    private TableColumn<Archived, String> col_cost;
    @FXML
    private TableColumn<Archived, String> col_date;
    @Autowired
    ArchivedRepository archivedRepository;

    public ManufacturingprojectApplication main;

    ObservableList<Archived> archivedList = FXCollections.observableArrayList();

    /**
     * Function sets column values
     */
    @FXML
    private void initialize(){
        //set up table columns
        col_name.setCellValueFactory(new PropertyValueFactory<>("archivedName"));
        col_cost.setCellValueFactory(new PropertyValueFactory<>("archivedMoney"));
        col_date.setCellValueFactory(new PropertyValueFactory<>("archivedDate"));


        loadData();
        table.setItems(archivedList);
        table.setEditable(true);
        col_name.setCellFactory(TextFieldTableCell.forTableColumn());
        col_cost.setCellFactory(TextFieldTableCell.forTableColumn());
        col_date.setCellFactory(TextFieldTableCell.forTableColumn());

    }

    /**
     * This function loads the data from the archive table
     */
    public void loadData(){
        for(Archived archived: archivedRepository.findAll()) {
            if(!archivedList.contains(archived))
            {
                archivedList.add(new Archived(archived.getArchivedName(), archived.getArchivedMoney(), archived.getArchivedDate()));
            }
        }

    }

    /**
     * @param actionEvent
     * Function takes in an ActionEvent parmeter
     * @throws IOException
     * Function returns user to the main screen
     */
    public void backBtn(ActionEvent actionEvent) throws IOException {
        main.showMainScreen();
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
