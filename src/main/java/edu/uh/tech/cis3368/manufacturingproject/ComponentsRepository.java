package edu.uh.tech.cis3368.manufacturingproject;

import org.springframework.data.repository.CrudRepository;

public interface ComponentsRepository extends CrudRepository<Components, Integer> {

    Components findById(int ID);
}
