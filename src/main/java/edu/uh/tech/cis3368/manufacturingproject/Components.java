package edu.uh.tech.cis3368.manufacturingproject;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Components {
    private int componentId;
    private String componentName;
    private String cost;

    public Components(){

    }

    public Components(String name, String cost){
        this.componentName = name;
        this.cost = cost;
    }

    public Components(int ID, String name, String cost){
        this.componentId = ID;
        this.componentName = name;
        this.cost = cost;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "component_id", nullable = false)
    public int getComponentId() {
        return componentId;
    }

    public void setComponentId(int componentId) {
        this.componentId = componentId;
    }

    @Basic
    @Column(name = "component_name", nullable = false, length = 50)
    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    @Basic
    @Column(name = "cost", nullable = false, length = 24)
    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Components that = (Components) o;
        return componentId == that.componentId &&
                Objects.equals(componentName, that.componentName) &&
                Objects.equals(cost, that.cost);
    }

    @Override
    public int hashCode() {

        return Objects.hash(componentId, componentName, cost);
    }
}
